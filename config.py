#!!! Edit this file and rename/copy it to "config.py"

# WLAN Configuration
WLAN_SSID = 'Toby2.4'
WLAN_PSK = '9724156277'

# MQTT Configuration
BROKER = '192.168.1.100'
CLIENT_ID_PREFIX = 'sonoff_switch'
LOG_TOPIC_PREFIX = 'logs/{}/'.format(CLIENT_ID_PREFIX)
LAST_WILL = 'Sorry, got to go without notice. See you soon!'
ACTUATOR_BASE_TOPIC = '/actuators/{}/'.format(CLIENT_ID_PREFIX)

# Software Settings
LED_DISPLAY_RELAY_STATE = True
